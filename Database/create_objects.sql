CREATE TABLE MarcacaoPonto (
  Id INTEGER PRIMARY KEY IDENTITY,
  CodigoRonda INTEGER,
  DataMarcacao DATETIME,
  MarcacaoRonda BIT DEFAULT 'FALSE',
  MarcacaoUsuario BIT DEFAULT 'FALSE',
  SolicitacaoAjuste BIT DEFAULT 'FALSE',
)

CREATE TABLE DataSituacao (
  Id INTEGER PRIMARY KEY IDENTITY,
  CodigoRonda INTEGER,
  DataPonto DATE,
  Ofensiva INTEGER,
  Situacao INTEGER,
  Ofensiva BIT DEFAULT 'FALSE',
  DiferencaJiraEmSegundos INTEGER,
  TotalRondaSegundos INTEGER,
  TotalJiraEmSegundos INTEEmGER
)
-- 1 OK 
-- 2 Marcacao Invalida
-- 3 Aguardando Aprovacao

---Tabela Lancamentos Rapido
CREATE TABLE JiraLancamentoRapido (
  IdJiraResumoLancamento INTEGER PRIMARY KEY IDENTITY,
  IdJira INTEGER,
  IdTarefa INTEGER,
  DescricaoLancamento VARCHAR(MAX),
  IdUsuario
)

ALTER TABLE JiraLancamentoRapido WITH NOCHECK ADD CONSTRAINT FK_JiraLancamento_Usuario FOREIGN KEY(IdUsuario)
REFERENCES Usuario(IdUsuario)

---Tabela de Usuarios
CREATE TABLE Usuario(
  IdUsuario INTEGER PRIMARY KEY IDENTITY,
  CodigoRonda INTEGER,
  UsuarioJira VARCHAR(MAX),
  PasswordJira VARCHAR(MAX),
  Email VARCHAR(MAX),
  UtilizaSprint BIT DEFAULT 'FALSE',
  EmailAjuste VARCHAR(MAX),
  ContadorOfensiva INTEGER
)

---Tabela de Filtro de Quadro por Usuario
CREATE TABLE UsuarioFiltroQuadro (
  IdQuadro INTEGER PRIMARY KEY IDENTITY,
  IdQuadroJira INTEGER NOT NULL,
  IdUsuario INTEGER NOT NULL
)

/*
ALTER TABLE UsuarioFiltroQuadro WITH NOCHECK ADD CONSTRAINT FK_FiltroQuadro_Usuario FOREIGN KEY(IdUsuario)
REFERENCES Usuario(IdUsuario)
*/

---Tabela de Filtro de Epico por Usuario
CREATE TABLE UsuarioFiltroEpico (
  IdEpico INTEGER PRIMARY KEY IDENTITY,
  IdQuadro INTEGER NOT NULL,
  IdUsuario INTEGER NOT NULL
)

---Tabela de Filtro de Jira por Usuario
CREATE TABLE UsuarioFiltroJira (
  IdJira INTEGER PRIMARY KEY IDENTITY,
  IdQuadro INTEGER NOT NULL,
  IdUsuario INTEGER NOT NULL
)

---Tabela de Filtro de Jira por Tarefa
CREATE TABLE UsuarioFiltroTarefa (
  IdTarefa INTEGER PRIMARY KEY IDENTITY,
  IdJira INTEGER NOT NULL,
  IdUsuario INTEGER NOT NULL
)