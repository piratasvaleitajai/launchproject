/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        var self = this;
        $(document).ready(function() {
            // are we running in native app or in a browser?
            window.isphone = false;
            if(document.URL.indexOf("file://") === -1 
                && document.URL.indexOf("http://") === -1 
                && document.URL.indexOf("https://") === -1) {
                window.isphone = true;
            }

            if( window.isphone ) {
                document.addEventListener("deviceready", self.onDeviceReady, false);
            } else {
                self.onDeviceReady();
            }
        });
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // $('.button-collapse').sideNav({
        //     menuWidth: 300, // Default is 240
        //     edge: 'left', // Choose the horizontal origin
        //     closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        // });
        
        $('#modalAvisos').modal();

        var self = this;
        $('#botaoLogin').click(function(event){
            event.preventDefault();

            var usuario = $('#usuarioLogin').val();
            var senha = $('#senhaLogin').val();

            $.post('http://192.168.2.58:8001/autenticacao/login', {
                usuarioJira: usuario,
                senha: senha
            }).done(function(dados){
                window.token = dados.token;
                $('#paginaLogin').hide();
                $('#paginaPrincipal').show();
                if(!dados.configuracoes) {
                    self.abrirConfiguracoes();
                    $('#botaoCancelarConfiguracoes').hide();
                } else {
                    self.atualizarConfiguracoes(dados.configuracoes);
                }
            }).fail(function(erro){
                self.mostrarAviso(
                    'Tentativa de login',
                    'Não foi possível efetuar o login: ' + erro.statusText,
                    'Ok');
            });
        });

        $('#botaoSalvarConfiguracoes').click(function(event){
            event.preventDefault();
            self.salvarConfiguracoes();
        });

        $('#botaoCancelarConfiguracoes').click(function(event){
            event.preventDefault();
            self.fecharConfiguracoes();
        });

        $('#botaoTopoConfiguracoes').click(function(event){
            event.preventDefault();
            self.abrirConfiguracoes();
        });
    },

    salvarConfiguracoes: function(){
        var seOrientarPorSprint = $('#seOrientarPorSprint:checked').length > 0;
        var codigoMatricula = $('#codigoMatricula').val();
        var emailUsuario = $('#emailUsuario').val();

        $.ajax({
            url: 'http://192.168.2.58:8001/configuracoes',
            type: 'post',
            data: {
                seOrientarPorSprint: seOrientarPorSprint,
                codigoMatricula: codigoMatricula,
                emailUsuario: emailUsuario
            },
            headers: {
                Token: window.token
            },
            dataType: 'json',
            success: function (data) {
                console.info(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    },

    atualizarConfiguracoes: function(configuracoes){
        if(configuracoes.seOrientarPorSprint){
            $('#seOrientarPorSprint').prop('checked', true);
            $('#seOrientarPorSprint').attr("checked", true);
        }
        $('#codigoMatricula').val(configuracoes.codigoMatricula);
        $('#emailUsuario').val(configuracoes.emailUsuario);
        // listaQuadrosConfiguracoes
        //<li class="collection-item">WMS 2nd<i class="iconeListaQuadros red-text material-icons">delete</i></li>
    },

    abrirConfiguracoes: function(){
        $('#paginaConfiguracoes').show('slide', { direction: 'left' }, 500);
        $('#cabecalhoPrincipal').hide();
        $('#botaoPrincipal').hide();
    },

    fecharConfiguracoes: function(){
        $('#paginaConfiguracoes').hide('slide', { direction: 'left' }, 500);
        $('#cabecalhoPrincipal').show();
        $('#botaoPrincipal').show();
    },

    mostrarAviso: function(titulo, conteudo, botaoOk){
        $('#modalAvisos').modal('open');
        $('#modalAvisosTitulo').html(titulo);
        $('#modalAvisosConteudo').html(conteudo);
        $('#modalAvisosBotaoOk').html(botaoOk);
    }
};
