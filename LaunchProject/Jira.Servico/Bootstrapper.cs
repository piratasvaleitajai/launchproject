﻿using System;
using Core.Consolidacao;
using Core.Autenticacao;
using Core.Configuracoes;
using Core.CruzamentoInformacoes;
using Core.Jira;
using Core.Ponto;
using Core.Ponto.Correcoes;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace Jira.Servico
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            pipelines.AfterRequest += HabilitarCORS;
            pipelines.OnError += (ctx, ex) => ErrorHandler(ex, ctx);
        }

        private Response ErrorHandler(Exception exception, NancyContext ctx)
        {
            HabilitarCORS(ctx);
            return null;
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);

            container.Register<IClienteJiraFactory, ClienteJiraFactory>();
            container.Register<IBuscadorJiras, BuscadorJiras>();
            container.Register<IConsolidadorLancamentos, ConsolidadorLancamentos>();
            container.Register<ICruzadorInformacoes, CruzadorInformacoes>();
            container.Register<IBuscadorMarcacoesBanco, BuscadorMarcacoesBanco>();
            container.Register<IAutenticadorJira, AutenticadorJira>();
            container.Register<IPersistidorDiagnosticos, PersistidorDiagnosticos>();
            container.Register<IBuscadorSituacaoDia, BuscadorSituacaoDia>();
            container.Register<IBuscadorUsuario, BuscadorUsuario>();
            container.Register<IAtualizadorUsuario, AtualizadorUsuario>();
            container.Register<IGerenciadorConfiguracoes, GerenciadorConfiguracoes>();
            container.Register<IGerenciadorMarcacoes, GerenciadorMarcacoes>();
        }

        private void HabilitarCORS(NancyContext ctx)
        {
            ctx.Response?.Headers?.Add("Access-Control-Allow-Origin", "*");
            ctx.Response?.Headers?.Add("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Token");
            ctx.Response?.Headers?.Add("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE");
        }
    }
}