﻿using System.Linq;
using Core.Consolidacao;
using Core.Jira;
using Nancy;

namespace Jira.Servico.Quadros
{
    public class QuadrosModule : NancyModule
    {
        public QuadrosModule(IBuscadorUsuario buscadorUsuario, IClienteJiraFactory clienteJiraFactory)
        {
            Get("/quadros/{pesquisa}", args =>
            {
                var token = Request.Headers.FirstOrDefault(x => x.Key == "token");
                var usuario = buscadorUsuario.ObterPorId(int.Parse(token.Value.FirstOrDefault()));
                var clienteJira = clienteJiraFactory.Novo(usuario.UsuarioJira, usuario.PasswordJira);
                var pesquisaUpper = ((string)args.pesquisa).ToUpper();
                var quadros = clienteJira.ObterQuadros().Where(x => x.name.ToUpper().Contains(pesquisaUpper));
                return quadros.Select(x => new QuadrosPesquisados
                {
                    Id = x.id,
                    Descricao = x.name
                }).ToList();
            });
        }
    }
}