﻿namespace Jira.Servico.Infraestrutura
{
    public interface IServiceControl
    {
        void Start();
        void Stop();
    }
}