﻿using System;
using System.Reflection;
using log4net;
using Microsoft.Owin.Hosting;

namespace Jira.Servico.Infraestrutura
{
    public class NancyServiceControl<T> : IServiceControl
    {
        private readonly ILog _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string _baseUrl;
        private IDisposable _webApp;

        public NancyServiceControl(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public NancyServiceControl() { }

        public void Start()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            var url = _baseUrl != null ? $"http://+:8001/{_baseUrl}" : "http://+:8001/";
            var startOptions = new StartOptions(url);
            _webApp = WebApp.Start<T>(startOptions);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _logger.Error($"Erro não tratado: {sender} - {e}");
        }

        public void Stop()
        {
            _webApp.Dispose();
        }
    }
}