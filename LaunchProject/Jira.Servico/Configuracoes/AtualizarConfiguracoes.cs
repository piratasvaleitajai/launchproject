﻿using System.Collections.Generic;

namespace Jira.Servico.Configuracoes
{
    public class AtualizarConfiguracoes
    {
        public bool SeOrientarPorSprint { get; set; }
        public int CodigoMatricula { get; set; }
        public string EmailUsuario { get; set; }
        public IList<int> IdsQuadros { get; set; }
    }
}