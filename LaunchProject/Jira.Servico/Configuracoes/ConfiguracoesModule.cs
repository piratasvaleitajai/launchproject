﻿using System.Linq;
using Core.Configuracoes;
using Core.Consolidacao;
using Nancy;
using Nancy.ModelBinding;

namespace Jira.Servico.Configuracoes
{
    public class ConfiguracoesModule : NancyModule
    {
        public ConfiguracoesModule(IBuscadorUsuario buscadorUsuario, IGerenciadorConfiguracoes gerenciadorConfiguracoes)
        {
            Post("configuracoes", args =>
            {
                var token = Request.Headers.FirstOrDefault(x => x.Key.ToLower() == "token");
                var usuario = buscadorUsuario.ObterPorId(int.Parse(token.Value.FirstOrDefault()));
                var atualizarConfiguracoes = this.Bind<AtualizarConfiguracoes>();
                gerenciadorConfiguracoes.Atualizar(
                    usuario.IdUsuario,
                    atualizarConfiguracoes.SeOrientarPorSprint,
                    atualizarConfiguracoes.CodigoMatricula,
                    atualizarConfiguracoes.EmailUsuario,
                    atualizarConfiguracoes.IdsQuadros);
                return 200;
            });
        }
    }
}