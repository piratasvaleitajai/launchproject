﻿using System;
using System.Reflection;
using Jira.Servico.Infraestrutura;
using log4net;
using Topshelf;

namespace Jira.Servico
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<IServiceControl>(s =>
                {
                    s.ConstructUsing(service => new NancyServiceControl<Startup>());
                    s.WhenStarted(StartApplication);
                    s.WhenStopped(StopApplication);
                });
                x.RunAsLocalSystem();
                x.StartAutomatically();

                x.SetDisplayName("JIRA HBSis");
                x.SetServiceName("Hbsis.Hira.Servico");
                x.SetDescription("Serviço do aplicativo para facilitar apontamentos do JIRA.");
            });
        }

        private static void StartApplication(IServiceControl serviceControl)
        {
            try
            {
                Logger.Info("Iniciando aplicação");
                serviceControl.Start();
                Logger.Info("Aplicação iniciada");
            }
            catch (Exception ex)
            {
                Logger.Error("Erro ao iniciar aplicação.", ex);
                throw;
            }
        }

        private static void StopApplication(IServiceControl serviceControl)
        {
            try
            {
                Logger.Info("Parando aplicação");
                serviceControl.Stop();
                Logger.Info("Aplicação parada");
            }
            catch (Exception ex)
            {
                Logger.Error("Erro ao parar aplicação.", ex);
                throw;
            }
        }
    }
}