﻿using Core;
using Core.Autenticacao;
using Nancy;
using Nancy.ModelBinding;

namespace Jira.Servico.Autenticacao
{
    public class AutenticacaoModule : NancyModule
    {
        public AutenticacaoModule(IAutenticadorJira autenticadorJira)
        {
            Post("autenticacao/login", args =>
            {
                var dadosAutenticacao = this.Bind<DadosAutenticacao>();
                UsuarioAutenticado usuarioAutenticado;
                try
                {
                    usuarioAutenticado = autenticadorJira.Autenticar(dadosAutenticacao.UsuarioJira, dadosAutenticacao.Senha);
                }
                catch (HttpException e)
                {
                    return Negotiate
                        .WithStatusCode(e.StatusCode)
                        .WithReasonPhrase(e.Message);
                }
                return usuarioAutenticado;
            });
        }
    }
}