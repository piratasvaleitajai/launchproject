﻿using System.ComponentModel.DataAnnotations;

namespace Jira.Servico.Autenticacao
{
    public class DadosAutenticacao
    {
        [Required]
        public string UsuarioJira { get; set; }
        [Required]
        public string Senha { get; set; }
    }
}