﻿using System;
using System.Globalization;
using System.Linq;
using Core;
using Core.Consolidacao;
using Core.Jira;
using Core.Jira.API;
using Nancy;
using Nancy.ModelBinding;

namespace Jira.Servico.Modules
{
    public class JiraModule : NancyModule
    {
        private readonly IBuscadorUsuario _buscadorUsuario;

        public JiraModule(IBuscadorJiras buscadorJiras, IBuscadorUsuario buscadorUsuario) 
            : base("jira")
        {
            _buscadorUsuario = buscadorUsuario;
            Get("/jiras/{id:int}", args =>
            {
                var credencial = ObterCredencial();
                var jira = buscadorJiras.ObterJira((int)args.Id, credencial);
                return Response.AsJson(new
                {
                    Id = jira.id,
                    Descricao = jira.fields.summary,
                    Epico = jira.fields.epic,
                    Pontuacao = jira.fields.customfield_10712.Replace(".0", "")
                });
            });
            
            Get("jiras/meus", args =>
            {
                var filtro = this.Bind<FiltroJiras>();
                var credencial = ObterCredencial();
                var atividade = buscadorJiras.BuscarJirasUsuario(credencial, filtro);
//                var atividade1 = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras());
//                var atividade2 = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {FiltrarUsuario = true});
//                var atividade3 = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {Inicio = DateTime.Today.AddDays(-10), FiltrarUsuario = true});
//                var atividade3b = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {Inicio = DateTime.Today.AddDays(-10)});
//                var atividade4 = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {Fim = DateTime.Today.AddDays(-10)});
//                var atividade4b = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {Fim = DateTime.Today.AddDays(-10), FiltrarUsuario = true});
//                var atividade5 = buscadorJira.BuscarJirasUsuario(credencial, new FiltroJiras {Inicio = DateTime.Today.AddDays(-30), Fim = DateTime.Today.AddDays(-3), FiltrarUsuario = true});
                
                return Response.AsJson(atividade);
            });
            
            Get("/quadros", args =>
            {
                var credencial = ObterCredencial();
                var quadros = buscadorJiras.ObterQuadros(credencial);
                return Response.AsJson(quadros);
            });
            
            Get("/quadros/{id:int}/sprints", args =>
            {
                var credencial = ObterCredencial();
                var sprints = buscadorJiras.ObterSprintsQuadro((int)args.Id, credencial);
                return Response.AsJson(sprints);
            });
            
            Get("/quadros/{id:int}/jiras", args =>
            {
                var credencial = ObterCredencial();
                var jiras = buscadorJiras.ObterJirasSprint((int)args.Id, credencial);
                return Response.AsJson(jiras);
            });
            
            Get("/sprints/{id:int}/jiras", args =>
            {
                var credencial = ObterCredencial();
                var jiras = buscadorJiras.ObterJirasSprint((int)args.Id, credencial);
                return Response.AsJson(jiras);
            });
            
            Get("/lancamentos", args =>
            {
                var credencial = ObterCredencial();
                var inicioPeriodo = DateTime.Today.InicioPeriodoAnterior();
                var jiras = buscadorJiras.ObterLancamentosDoUsuario(inicioPeriodo, credencial);
                return Response.AsJson(jiras);
            });
        }
        
        private Credencial ObterCredencial()
        {
            var token = Request.Headers.FirstOrDefault(x => x.Key == "token");
            var usuario = _buscadorUsuario.ObterPorId(int.Parse(token.Value.FirstOrDefault()));
            return new Credencial(usuario);
        }
    }
}