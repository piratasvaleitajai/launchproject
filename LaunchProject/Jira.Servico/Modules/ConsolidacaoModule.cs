﻿using System;
using System.Linq;
using Core;
using Core.Consolidacao;
using Core.Jira;
using Core.Ponto;
using Nancy;

namespace Jira.Servico.Modules
{
    public class ConsolidacaoModule : NancyModule
    {
        private readonly IBuscadorUsuario _buscadorUsuario;

        public ConsolidacaoModule(
            IConsolidadorLancamentos consolidadorLancamentos,
            IBuscadorSituacaoDia buscadorSituacaoDia,
            IBuscadorUsuario buscadorUsuario,
            IBuscadorMarcacoesBanco buscadorMarcacoesBanco
        ) : base("consolidacao")
        {
            _buscadorUsuario = buscadorUsuario;
            
            Get("/pendencias", args =>
            {
                ObterCredencial();

                var situacoesComPendencias = buscadorSituacaoDia.BuscarSituacoesComPendencias(DateTime.Today.InicioPeriodoAnterior(), ObterCredencial())
                    .Select(x =>
                    {
                        var marcacoes = buscadorMarcacoesBanco.BuscarMarcacoesDoDia(x.DataPonto.Date);
                        return new
                        {
                            Id = x.Id,
                            Data = x.DataPonto,
                            Situacao = x.Situacao,
                            Tempos = new
                            {
                                Ronda = Formatar(x.TotalRondaEmSegundos),
                                Jira = Formatar(x.TotalJiraEmSegundos),
                                Diferenca = Formatar(x.DiferencaJiraEmSegundos)
                            },
                            Marcacoes = marcacoes.Select(m => new
                            {
                                m.Id,
                                m.MarcacaoRonda,
                                m.MarcacaoUsuario,
                                m.SolicitacaoAjuste
                            })
                        };
                    });
                return Response.AsJson(situacoesComPendencias);
            });
            
            Get("/ofensiva", args =>
            {
                var ofensiva = buscadorUsuario.BuscarOfensivaAtual(ObterCredencial().CodigoRonda);
                return Response.AsJson(new
                {
                    Ofensiva = ofensiva
                });
            });
            
            Post("/consolidar", args =>
            {
                consolidadorLancamentos.ConsolidarUltimoPeriodo(ObterCredencial());
                return "";
            });
        }

        private static string Formatar(int segundos)
        {
            var t = TimeSpan.FromSeconds(segundos);
            return $"{t.Hours.ToString().PadLeft(2, '0')}:{t.Minutes.ToString().PadLeft(2, '0')}";
        }

        private Credencial ObterCredencial()
        {
            var token = Request.Headers.FirstOrDefault(x => x.Key == "token");
            var usuario = _buscadorUsuario.ObterPorId(int.Parse(token.Value.FirstOrDefault()));
            return new Credencial(usuario);
        }
    }
}