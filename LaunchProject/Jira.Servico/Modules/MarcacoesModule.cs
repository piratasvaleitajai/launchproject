﻿using System.Linq;
using Core.Consolidacao;
using Core.Jira;
using Core.Ponto.Correcoes;
using Jira.Servico.Modules.Models;
using Nancy;
using Nancy.ModelBinding;

namespace Jira.Servico.Modules
{
    public class MarcacoesModule : NancyModule
    {
        private readonly IBuscadorUsuario _buscadorUsuario;

        public MarcacoesModule(
            IGerenciadorMarcacoes gerenciadorMarcacoes, 
            IConsolidadorLancamentos consolidadorLancamentos,
            IBuscadorUsuario buscadorUsuario) 
            : base("marcacoes")
        {
            _buscadorUsuario = buscadorUsuario;
            
            Post("/marcacao-corretiva", args =>
            {
                var inputCorrecao = this.Bind<InputCorrecaoMarcacao>();
                var credencial = ObterCredencial();
                
                gerenciadorMarcacoes.AdicionarMarcacao(credencial.CodigoRonda, inputCorrecao.DataHora);
                consolidadorLancamentos.ConsolidarUltimoPeriodo(credencial);
                
                return true;
            });
        }
        
        private Credencial ObterCredencial()
        {
            var token = Request.Headers.FirstOrDefault(x => x.Key == "token");
            var usuario = _buscadorUsuario.ObterPorId(int.Parse(token.Value.FirstOrDefault()));
            return new Credencial(usuario);
        }
    }
}