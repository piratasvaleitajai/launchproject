﻿using System;

namespace Core
{
    public class HttpException : Exception
    {
        public int StatusCode { get; }

        public HttpException(int statusCode)
        {
            StatusCode = statusCode;
        }

        public HttpException(int statusCode, string mensagem) : base(mensagem)
        {
            StatusCode = statusCode;
        }

        public HttpException(int statusCode, string mensagem, Exception exception) : base(mensagem, exception)
        {
            StatusCode = statusCode;
        }
    }
}