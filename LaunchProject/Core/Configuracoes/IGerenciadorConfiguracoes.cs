﻿using System.Collections.Generic;

namespace Core.Configuracoes
{
    public interface IGerenciadorConfiguracoes
    {
        void Atualizar(int idUsuario, bool utilizaSprint, int codigoRonda, string email, IList<int> idsQuadros);
    }
}