﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Core.Consolidacao;
using Core.Ponto;
using Dapper;

namespace Core.Configuracoes
{
    public class GerenciadorConfiguracoes : IGerenciadorConfiguracoes
    {
        public void Atualizar(int idUsuario, bool utilizaSprint, int codigoRonda, string email, IList<int> idsQuadros)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var usuario = connection
                .Query<Usuario>($"select * from Usuario where IdUsuario = '{idUsuario}'")
                .FirstOrDefault();

            if (usuario != null)
            {
                connection.Execute(
                    "Update Usuario Set UtilizaSprint = @UtilizaSprint, CodigoRonda = @CodigoRonda, Email = @Email where IdUsuario = @IdUsuario",
                    new {UtilizaSprint = utilizaSprint, CodigoRonda = codigoRonda, Email = email, usuario.IdUsuario});

                connection.Execute($"delete from UsuarioFiltroQuadro where IdUsuario = {usuario.IdUsuario}");

                if (idsQuadros != null && idsQuadros.Any())
                {
                    connection.Execute(@"insert UsuarioFiltroQuadro(IdQuadroJira, IdUsuario) values (@IdQuadro, @IdUsuario)",
                        idsQuadros.Select(x => new
                        {
                            IdQuadro = x,
                            usuario.IdUsuario
                        }));
                }
            }
            else
            {
                throw new HttpException(400, "Problema ao atualizar configurações do usuário.");
            }
            connection.Close();
        }
    }
}