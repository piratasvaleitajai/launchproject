using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Jira.API
{
    public interface IClienteJira
    {
        JiraBoard ObterQuadro(int idQuadro);

        IList<JiraBoard> ObterQuadros();
        IList<JiraSprint> ObterSprintsDoQuadro(int idQuadro);
        IList<JiraIssue> ObterJirasDoQuadro(int idQuadro);
        IList<JiraIssue> ObterJirasNaSprint(int idSprint);
        JiraIssue ObterJira(int idProblema);

        IList<Worklog> ObterLancamentos(string idJira);
        IList<JiraIssue> ObterJirasDoUsuarioComLancamentos(DateTime dataInicio);
        Task<bool> RealizarLancamento(Worklog lancamento, TimeSpan horario);

        IList<JiraIssue> BuscarJirasUsuario(FiltroJiras filtro);
    }
}