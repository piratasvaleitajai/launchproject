﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.Jira.API
{
    public class ClienteJira : IClienteJira
    {
        //http://jirahml.hbsis.com.br
        private const string Host = "http://jirahml.hbsis.com.br";
        private readonly string _nomeUsuario;
        private readonly string _senha;

        public ClienteJira(string nomeUsuario, string senha)
        {
            _nomeUsuario = nomeUsuario;
            _senha = senha;
        }

        private HttpClient ObterClienteHttp()
        {
            var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes($"{_nomeUsuario}:{_senha}");
            client.BaseAddress = new Uri(Host);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public IList<JiraIssue> BuscarJirasUsuario(FiltroJiras filtro)
        {
            var listaFiltros = new List<string>();
            
            if (filtro.FiltrarUsuario)
                listaFiltros.Add($"assignee={_nomeUsuario}");
            
            if (filtro.Status.HasValue)
                listaFiltros.Add($"status={filtro.Status.ToString()}"); //TODO corrigir filtro
            
            if (filtro.Inicio.HasValue)
                listaFiltros.Add($"created>={filtro.Inicio.Value:yyyy-MM-dd}");

            if (filtro.Fim.HasValue)
                listaFiltros.Add($"updated<={filtro.Fim.Value:yyyy-MM-dd}");
            
            var jql = string.Join("+and+", listaFiltros);
            var url = $"rest/api/2/search?jql={jql}";
            var jiraResult = Ler<JiraIssuesResult<JiraIssue>>(url);
            return jiraResult.issues ?? new JiraIssue[0];
        }

        private T Ler<T>(string url)
        {
            var client = ObterClienteHttp();
            var result = client.GetAsync(url).Result;
            if (result.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new HttpException(401, "Erro ao validar login no Jira.");
            }
            var content = result.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(content.Result);
        }

        public JiraBoard ObterQuadro(int idQuadro)
        {
            var url = $"/rest/agile/1.0/board/{idQuadro}";
            var jiraResult = Ler<JiraBoard>(url);
            return jiraResult;
        }

        public IList<JiraBoard> ObterQuadros()
        {
            var boardList = new List<JiraBoard>();
            bool last;
            long indexPagination=0;
            do {
                var url = $"/rest/agile/1.0/board?startAt={indexPagination}";
                var jiraResult = Ler<JiraResult<JiraBoard>>(url);
                boardList.AddRange(jiraResult.values);
                last = jiraResult.isLast;
                if (!last) {
                    indexPagination += 50;
                }
            } while (!last);
            
            return boardList;
        }

        public IList<JiraSprint> ObterSprintsDoQuadro(int idQuadro)
        {
            var url = $"/rest/agile/1.0/board/{idQuadro}/sprint?jql=order%20by%20created%20desc";
            var jiraResult = Ler<JiraResult<JiraSprint>>(url);
            return jiraResult.values;
        }
        
        public IList<JiraIssue> ObterJirasDoQuadro(int idQuadro)
        {
            var url = $"/rest/agile/1.0/board/{idQuadro}/issue?maxResults=1000&jql=order%20by%20created%20desc";
            var jiraResult = Ler<JiraIssuesResult<JiraIssue>>(url);
            return jiraResult.issues;
        }

        public IList<JiraIssue> ObterJirasNaSprint(int idSprint)
        {
            var url = $"/rest/agile/1.0/sprint/{idSprint}/issue?maxResults=1000&?jql=order%20by%20created%20desc";
            var jiraResult = Ler<JiraIssuesResult<JiraIssue>>(url);
            return jiraResult.issues;
        }

        public JiraIssue ObterJira(int idProblema)
        {
            var url = $"/rest/agile/1.0/issue/{idProblema}";
            var jiraResult = Ler<JiraIssue>(url);
            return jiraResult;
        }

        public IList<Worklog> ObterLancamentos(string idJira)
        {
            var url = $"/rest/api/2/issue/{idJira}/worklog";
            var jiraResult = Ler<JiraResult<Worklog>>(url);
            return jiraResult.values ?? new Worklog[0];
        }
        
        public IList<JiraIssue> ObterJirasDoUsuarioComLancamentos(DateTime dataInicio)
        {
            var listaFiltros = new[]
            {
                $"worklogAuthor={_nomeUsuario}",
                $"worklogDate>={(dataInicio-DateTime.Today.Date).Days}d",
            };
            var jql = string.Join("+and+", listaFiltros);
            var url = $"rest/api/2/search?jql={jql}&fields=worklog";
            var jiraResult = Ler<JiraIssuesResult<JiraIssue>>(url);
            
            return jiraResult.issues ?? new JiraIssue[0];
        }

        public async Task<bool> RealizarLancamento(Worklog lancamento, TimeSpan horario)
        {
            try
            {
                var worklogJson = JsonConvert.SerializeObject(new
                {
                    comment = "",
                    started = lancamento.Date.ToString("yyyy-MM-dd") + $"T{horario.Hours.ToString().PadLeft(2, '0')}:{horario.Minutes.ToString().PadLeft(2, '0')}:00.000-0300",
                    timeSpentSeconds = lancamento.TimeSpentSeconds
                });

                var client = ObterClienteHttp();
                var url = $"/rest/api/2/issue/{lancamento.JiraCode}/worklog";
                var content = new StringContent(worklogJson, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
                return result.StatusCode == HttpStatusCode.Created;
            }
            catch
            {
                //TODO logar
                return false;
            }
        }
    }

    public class FiltroJiras
    {
        public bool FiltrarUsuario { get; set; }
        public StatusJira? Status { get; set; }
        public DateTime? Inicio { get; set; }
        public DateTime? Fim { get; set; }
    }

    public enum StatusJira
    {
        [Description("new")]
        NaoIniciado = 0,
        
        [Description("in progress")]
        EmProgresso = 1,
        
        [Description("finished")]
        Concluido = 2,
        
        [Description("cancelled")]
        Cancelado = 3
    }
}
