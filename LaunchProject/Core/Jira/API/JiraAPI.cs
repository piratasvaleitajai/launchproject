﻿using System;
using System.Collections.Generic;

namespace Core.Jira.API
{
    public class JiraBoard
    {
        public int id { get; set; }
        public string self { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }

    public class JiraSprint
    {
        public int id { get; set; }
        public string self { get; set; }
        public string state { get; set; }
        public string name { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int originBoardId { get; set; }
    }

    public class JiraIssue
    {
        public int id { get; set; }
        public string self { get; set; }
        public string key { get; set; }
        public JiraIssueFields fields { get; set; }
    }

    public class JiraIssueFields
    {
        public JiraIssueEpic epic { get; set; }
        public string summary { get; set; }//Descrição estória
        public string customfield_10712 { get; set; } //Pontuação
        public IList<JiraIssueSubTasks> subtasks { get; set; }
        public JiraIssueWorklog worklog { get; set; }
        public JiraIssueType issuetype { get; set; }
    }

    public class JiraIssueType
    {
        public bool subtask { get; set; } //é subtarefa?
    }

    public class JiraIssueEpic
    {
        public string name { get; set; }
    }

    public class JiraIssueWorklog
    {
        public int total { get; set; }
        public IList<JiraIssueWorklogs> worklogs { get; set; }
    }

    public class JiraIssueWorklogs
    {
        public int timeSpentSeconds { get; set; }
        public Author Author { get; set; }
        public DateTime updated { get; set; }
        public DateTime started { get; set; }
        public string comment { get; set; }

    }

    public class JiraIssueSubTasks
    {
        public int id { get; set; }
        public string self { get; set; }
        public string key { get; set; }
        public JiraIssueFields fields { get; set; }
    }

    public class JiraIssueSubTasksFields
    {
        public string summary { get; set; }//Descrição subtarefa
    }

    public class JiraIssuesResult<T>
    {
        public int maxResults { get; set; }
        public int startAt { get; set; }
        public bool isLast { get; set; }
        public IList<T> issues { get; set; }
    }

    public class JiraResult<T>
    {
        public int maxResults { get; set; }
        public int startAt { get; set; }
        public bool isLast { get; set; }
        public IList<T> values { get; set; }
    }

    public class Worklog
    {
        public string JiraCode { get; set; }
        public int TimeSpentSeconds { get; set; }
        public DateTime Date { get; set; }
        public Author Author { get; set; }
    }

    public class Author
    {
        public string self { get; set; }
        public string name { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }
}
