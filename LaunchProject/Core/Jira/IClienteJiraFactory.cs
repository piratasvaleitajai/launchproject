﻿using Core.Jira.API;

namespace Core.Jira
{
    public interface IClienteJiraFactory
    {
        IClienteJira Novo(string usuario, string senha);
    }
}