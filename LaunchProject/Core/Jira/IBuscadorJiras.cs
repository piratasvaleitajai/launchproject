﻿using System;
using System.Collections.Generic;
using Core.Jira.API;

namespace Core.Jira
{
    public interface IBuscadorJiras
    {
//        AtividadeJira ObterLancamentosDaAtividade(string codigoJira, Credencial credencial);
        AtividadeJira ObterLancamentosDoUsuario(DateTime dataInicio, Credencial credencial);
        
        IList<JiraBoard> ObterQuadros(Credencial credencial);
        IList<JiraSprint> ObterSprintsQuadro(int idQuadro, Credencial credencial);
        IList<JiraIssue> BuscarJirasUsuario(Credencial credencial, FiltroJiras filtro);
        IList<JiraIssue> ObterJirasSprint(int idSprint, Credencial credencial);
        JiraIssue ObterJira(int id, Credencial credencial);
    }
}