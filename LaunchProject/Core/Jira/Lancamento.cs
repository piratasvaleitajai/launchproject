﻿using System;

namespace Core.Jira
{
    public class Lancamento
    {
        public TimeSpan TempoLancado { get; }
        public DateTime Data { get; }
        public string Comment { get; }
        public int IdJira { get; }

        public Lancamento(TimeSpan tempoLancado, DateTime data, string comment, int idJira)
        {
            TempoLancado = tempoLancado;
            Data = data;
            Comment = comment;
            IdJira = idJira;
        }
    }
}