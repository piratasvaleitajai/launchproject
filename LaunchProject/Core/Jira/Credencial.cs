using Core.Consolidacao;

namespace Core.Jira
{
    public class Credencial
    {
        public string Usuario { get; }
        public string Senha { get; }
        public int CodigoRonda { get; set; }

        public Credencial(string usuario, string senha, int codigoRonda)
        {
            Usuario = usuario;
            Senha = senha;
            CodigoRonda = codigoRonda;
        }

        public Credencial(Usuario usuario)
        {
            Usuario = usuario.Email;
            Senha = usuario.PasswordJira;
            CodigoRonda = usuario.CodigoRonda;
        }
    }
}