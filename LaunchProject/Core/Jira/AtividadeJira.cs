﻿using System.Collections.Generic;

namespace Core.Jira
{
    public class AtividadeJira
    {
        public IList<Lancamento> Lancamentos { get; }

        public AtividadeJira(IList<Lancamento> lancamentos)
        {
            Lancamentos = lancamentos;
        }
    }
}