﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core.Jira.API;

namespace Core.Jira
{
    public class BuscadorJiras : IBuscadorJiras
    {
        private readonly IClienteJiraFactory _clienteJiraFactory;

        public BuscadorJiras(IClienteJiraFactory clienteJiraFactory)
        {
            _clienteJiraFactory = clienteJiraFactory;
        }

//        public AtividadeJira ObterLancamentosDaAtividade(string codigoJira, Credencial credencial)
//        {
//            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
//            var worklogs = clienteJira.ObterLancamentos(codigoJira);
//            var lancamentos = worklogs
//                .Select(wl => new Lancamento(TimeSpan.FromSeconds(wl.timeSpentSeconds), wl.updated, wl.comment, jira.id)))
//                .ToArray();
//            return new AtividadeJira(lancamentos);
//        }

        public AtividadeJira ObterLancamentosDoUsuario(DateTime dataInicio, Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            var jiras = clienteJira.ObterJirasDoUsuarioComLancamentos(dataInicio)
                .Where(x => x.fields.worklog != null)
                .ToList();
            var lancamentos = jiras
                .SelectMany(jira => jira.fields.worklog.worklogs
                .Where(wl => wl.Author.name == credencial.Usuario)
                .Select(wl => new Lancamento(TimeSpan.FromSeconds(wl.timeSpentSeconds), wl.started, wl.comment, jira.id)))
                .ToList();
            return new AtividadeJira(lancamentos);
        }

        public IList<JiraBoard> ObterQuadros(Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            return clienteJira.ObterQuadros();
        }

        public IList<JiraSprint> ObterSprintsQuadro(int idQuadro, Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            return clienteJira.ObterSprintsDoQuadro(idQuadro);
        }
        
        public IList<JiraIssue> ObterJirasQuadro(int idQuadro, Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            return clienteJira.ObterJirasDoQuadro(idQuadro);
        }
        
        public IList<JiraIssue> BuscarJirasUsuario(Credencial credencial, FiltroJiras filtro)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            var jiras = clienteJira.BuscarJirasUsuario(filtro);
            return jiras;
        }

        public IList<JiraIssue> ObterJirasSprint(int idSprint, Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            var jiras = clienteJira.ObterJirasNaSprint(idSprint);
            return jiras;
        }

        public JiraIssue ObterJira(int id, Credencial credencial)
        {
            var clienteJira = _clienteJiraFactory.Novo(credencial.Usuario, credencial.Senha);
            var jira = clienteJira.ObterJira(id);
            return jira;
        }
    }
}