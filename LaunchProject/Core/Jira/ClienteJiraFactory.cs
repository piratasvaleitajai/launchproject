﻿using Core.Jira.API;

namespace Core.Jira
{
    public class ClienteJiraFactory : IClienteJiraFactory
    {
        public IClienteJira Novo(string usuario, string senha)
        {
            return new ClienteJira(usuario, senha);
        }
    }
}