﻿using System.Data.SqlClient;
using System.Linq;
using Core.Consolidacao;
using Core.Consolidacao;
using Core.Jira;
using Core.Jira.API;
using Dapper;

namespace Core.Autenticacao
{
    public class AutenticadorJira : IAutenticadorJira
    {
        private readonly IClienteJiraFactory _clienteJiraFactory;
        private readonly IBuscadorUsuario _buscadorUsuario;

        //TODO mudar a conexão
        public static string ConnectionString = "Server=192.168.2.56\\SQLEXPRESS;Database=HBJira;User Id=sa;Password=321654hb;";

        public AutenticadorJira(IClienteJiraFactory clienteJiraFactory, IBuscadorUsuario buscadorUsuario)
        {
            _clienteJiraFactory = clienteJiraFactory;
            _buscadorUsuario = buscadorUsuario;
        }

        public UsuarioAutenticado Autenticar(string usuarioJira, string senha)
        {
            var clienteJira = _clienteJiraFactory.Novo(usuarioJira, senha);

            var connection = new SqlConnection(ConnectionString);
            connection.Open();

            clienteJira.BuscarJirasUsuario(new FiltroJiras());

            var usuario = _buscadorUsuario.ObterPorUsuarioJira(usuarioJira);

            if (usuario != null)
            {
                var quadros = _buscadorUsuario.ObterQuadrosUsuarioPorIdUsuario(usuario.IdUsuario);

                return new UsuarioAutenticado
                {
                    Token = usuario.IdUsuario.ToString(),
                    Configuracoes = new ConfiguracoesUsuario
                    {
                        SeOrientarPorSprint = usuario.UtilizaSprint,
                        CodigoMatricula = usuario.CodigoRonda,
                        EmailUsuario = usuario.Email,
                        Quadros = quadros?.Select(x =>
                        {
                            var quadroJira = clienteJira.ObterQuadro(x.IdQuadro);

                            return new QuadroConfigurado
                            {
                                Id = x.IdQuadro,
                                Descricao = quadroJira.name
                            };
                        }).ToList()
                    }
                };
            }

            const string sql = @"
                insert Usuario(UsuarioJira, PasswordJira) values(@usuarioJira, @senha);
                SELECT CAST(SCOPE_IDENTITY() as int)";

            var id = connection.Query<int>(sql, new {usuarioJira, senha}).Single();

            return new UsuarioAutenticado
            {
                Token = id.ToString()
            };
        }
    }
}