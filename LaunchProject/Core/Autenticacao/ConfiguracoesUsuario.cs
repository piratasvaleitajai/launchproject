﻿using System.Collections.Generic;

namespace Core.Autenticacao
{
    public class ConfiguracoesUsuario
    {
        public bool SeOrientarPorSprint { get; set; }
        public int CodigoMatricula { get; set; }
        public string EmailUsuario { get; set; }
        public string EmailPara { get; set; }
        public string EmailComCopia { get; set; }
        public IList<QuadroConfigurado> Quadros { get; set; }
    }
}