﻿namespace Core.Autenticacao
{
    public class QuadroConfigurado
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}