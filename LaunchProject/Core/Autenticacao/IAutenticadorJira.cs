﻿namespace Core.Autenticacao
{
    public interface IAutenticadorJira
    {
        UsuarioAutenticado Autenticar(string usuarioJira, string senha);
    }
}