﻿namespace Core.Autenticacao
{
    public class UsuarioAutenticado
    {
        public string Token { get; set; }
        public ConfiguracoesUsuario Configuracoes { get; set; }
    }
}