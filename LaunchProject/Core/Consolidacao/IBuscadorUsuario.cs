using Core.Jira;
using System.Collections.Generic;

namespace Core.Consolidacao
{
    public interface IBuscadorUsuario
    {
        Usuario ObterPorId(int id);
        Usuario ObterPorUsuarioJiraIncluindoQuadros(string usuarioJira);
        Usuario ObterPorCodigoRonda(int codigoRonda);
        int BuscarOfensivaAtual(int codigoRonda);
        Usuario ObterPorUsuarioJira(string usuarioJira);
        IList<UsuarioFiltroQuadro> ObterQuadrosUsuarioPorIdUsuario(int idUsuario);

    }
}