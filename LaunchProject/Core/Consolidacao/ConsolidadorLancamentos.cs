using System;
using System.Collections.Generic;
using System.Linq;
using Core.CruzamentoInformacoes;
using Core.Jira;
using Core.Ponto;

namespace Core.Consolidacao
{
    public class ConsolidadorLancamentos : IConsolidadorLancamentos
    {
        private readonly ICruzadorInformacoes _cruzadorInformacoes;
        private readonly IBuscadorJiras _buscadorJiras;
        private readonly IBuscadorMarcacoesBanco _buscadorMarcacoesBanco;
        private readonly IPersistidorDiagnosticos _persistidorDiagnosticos;

        public ConsolidadorLancamentos(
            ICruzadorInformacoes cruzadorInformacoes,
            IBuscadorJiras buscadorJiras,
            IBuscadorMarcacoesBanco buscadorMarcacoesBanco,
            IPersistidorDiagnosticos persistidorDiagnosticos
        )
        {
            _persistidorDiagnosticos = persistidorDiagnosticos;
            _cruzadorInformacoes = cruzadorInformacoes;
            _buscadorJiras = buscadorJiras;
            _buscadorMarcacoesBanco = buscadorMarcacoesBanco;
        }

        public void ConsolidarUltimoPeriodo(Credencial credencial)
        {
            var inicioPeriodoAnterior = DateTime.Today.InicioPeriodoAnterior();
            var lancamentosUltimosTempos = _buscadorJiras.ObterLancamentosDoUsuario(inicioPeriodoAnterior, credencial);
            var marcacoesPonto = _buscadorMarcacoesBanco.BuscarMarcacoes(inicioPeriodoAnterior);

            var marcacoesAgrupadasPorData = marcacoesPonto
                .GroupBy(x => x.DataMarcacao.Date)
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x);
            var lancamentosAgrupadosPorData = lancamentosUltimosTempos.Lancamentos
                .GroupBy(x => x.Data.Date)
                .OrderBy(x => x.Key)
                .ToDictionary(x => x.Key, x => x);

            var diagnosticosPeriodo = new List<IDiagnostico>();

            var menorDataMarcacoes = marcacoesAgrupadasPorData.Keys.First().Date;
            var menorDataLancamentosJira = lancamentosAgrupadosPorData.Keys.First().Date;
            var menorData = menorDataMarcacoes < menorDataLancamentosJira ? menorDataMarcacoes : menorDataLancamentosJira;
            var data = menorData;
            while (data < DateTime.Today)
            {
                var marcacoesDoDia = marcacoesAgrupadasPorData.ContainsKey(data)
                    ? marcacoesAgrupadasPorData[data].ToArray()
                    : new MarcacaoPonto[0];
                var lancamentosDoDia = lancamentosAgrupadosPorData.ContainsKey(data)
                    ? lancamentosAgrupadosPorData[data].ToArray()
                    : new Lancamento[0];

                var tempos = marcacoesDoDia.Select(x => x.DataMarcacao.TimeOfDay).ToList();
                var marcacaoDiaria = new MarcacaoDiaria(tempos, data);
                var diagnostico = _cruzadorInformacoes.Cruzar(marcacaoDiaria, lancamentosDoDia);
                diagnosticosPeriodo.Add(diagnostico);

                data = data.AddDays(1);
            }

            PersistirDiagnosticos(diagnosticosPeriodo, credencial.CodigoRonda);
        }

        private void PersistirDiagnosticos(IEnumerable<IDiagnostico> diagnosticosPeriodo, int codigoRonda)
        {
            foreach (var diagnostico in diagnosticosPeriodo)
            {
                _persistidorDiagnosticos.Persistir(diagnostico, codigoRonda);
            }
        }
    }
}