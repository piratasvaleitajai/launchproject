using Core.CruzamentoInformacoes;

namespace Core.Consolidacao
{
    public interface IPersistidorDiagnosticos
    {
        void Persistir(IDiagnostico diagnostico, int codigoRonda);
    }
}