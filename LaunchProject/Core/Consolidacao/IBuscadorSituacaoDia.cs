﻿using System;
using System.Collections.Generic;
using Core.Jira;

namespace Core.Consolidacao
{
    public interface IBuscadorSituacaoDia
    {
        IList<DataSituacao> BuscarSituacoesComPendencias(DateTime dataInicio, Credencial credencial);
        int BuscarOfensivaAtual(Credencial credencial);
    }
}