using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Core.Ponto;
using Dapper;

namespace Core.Consolidacao
{
    public class BuscadorUsuario : IBuscadorUsuario
    {
        public Usuario ObterPorId(int id)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var usuario = connection
                .Query<Usuario>($"select * from Usuario where IdUsuario = {id}")
                .FirstOrDefault();
            connection.Close();
            return usuario;
        }

        public Usuario ObterPorUsuarioJiraIncluindoQuadros(string usuarioJira)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            
            var usuario = connection
                .Query<Usuario>($"select * from Usuario where UsuarioJira = '{usuarioJira}'")
                .FirstOrDefault();

            var quadros = connection
                .Query<UsuarioFiltroQuadro>($"select * from UsuarioFiltroQuadro where IdUsuario = {usuario.IdUsuario}")
                .ToList();

            usuario.Quadros = quadros;

            connection.Close();
            return usuario;
        }

        public Usuario ObterPorUsuarioJira(string usuarioJira)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            
            var usuario = connection
                .Query<Usuario>($"select * from Usuario where UsuarioJira = '{usuarioJira}'")
                .FirstOrDefault();
            
            connection.Close();
            return usuario;
        }

        public IList<UsuarioFiltroQuadro> ObterQuadrosUsuarioPorIdUsuario(int idUsuario)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            
            var quadros = connection
                .Query<UsuarioFiltroQuadro>($"select * from UsuarioFiltroQuadro where IdUsuario = {idUsuario}")
                .ToList();
            
            connection.Close();
            return quadros;
        }

        public Usuario ObterPorCodigoRonda(int codigoRonda)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var usuario = connection
                .Query<Usuario>($"select * from Usuario where CodigoRonda = {codigoRonda}")
                .FirstOrDefault();
            connection.Close();
            return usuario;
        }

        public int BuscarOfensivaAtual(int codigoRonda)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var contadorOfensiva = connection
                .Query<int?>($"select ContadorOfensiva from Usuario where CodigoRonda = {codigoRonda}")
                .FirstOrDefault() ?? 0;
            connection.Close();
            return contadorOfensiva;
        }
    }
}