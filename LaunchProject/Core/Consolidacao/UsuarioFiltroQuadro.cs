﻿namespace Core.Consolidacao
{
    public class UsuarioFiltroQuadro
    {
        public int IdQuadro { get; set; }
        public int IdQuadroJira { get; set; }
        public int IdUsuario { get; set; }
    }
}