using System.Collections.Generic;

namespace Core.Consolidacao
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public int CodigoRonda { get; set; }
        public string UsuarioJira { get; set; }
        public string PasswordJira { get; set; }
        public string Email { get; set; }
        public bool UtilizaSprint { get; set; }
        public string EmailAjuste { get; set; }
        public int ContadorOfensiva { get; set; }

        public IList<UsuarioFiltroQuadro> Quadros { get; set; }

        /*CREATE TABLE Usuario(
 IdUsuario INTEGER PRIMARY KEY IDENTITY,
 CodigoRonda INTEGER,
 UsuarioJira VARCHAR(MAX),
 PasswordJira VARCHAR(MAX),
 Email VARCHAR(MAX),
 UtilizaSprint BIT DEFAULT 'FALSE',
 EmailAjuste VARCHAR(MAX),
 ContadorOfensiva INTEGER
)
        */
    }
}