namespace Core.Consolidacao
{
    public interface IAtualizadorUsuario
    {
        void IncrementarOfensiva(int codigoRonda);
        void ZerarOfensiva(int codigoRonda);
    }
}