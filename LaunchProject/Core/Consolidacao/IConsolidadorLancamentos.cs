﻿using Core.Jira;

namespace Core.Consolidacao
{
    public interface IConsolidadorLancamentos
    {
        void ConsolidarUltimoPeriodo(Credencial obterCredencial);
    }
}