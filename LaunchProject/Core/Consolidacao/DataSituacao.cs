using System;

namespace Core.Consolidacao
{
    public class DataSituacao
    {
        public int Id { get; set; }
        public int CodigoRonda { get; set; }
        public DateTime DataPonto { get; set; }
        public int Situacao { get; set; }
        public bool Ofensiva { get; set; }
        public int DiferencaJiraEmSegundos { get; set; }
        public int TotalJiraEmSegundos { get; set; }
        public int TotalRondaEmSegundos { get; set; }
        /*
        CREATE TABLE DataSituacao (
 Id INTEGER PRIMARY KEY IDENTITY,
 CodigoRonda INTEGER,
 DataPonto DATE,
 DataSituacao INTEGER
)*/
    }
}