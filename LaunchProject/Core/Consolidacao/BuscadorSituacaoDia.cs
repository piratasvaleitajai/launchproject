using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Core.CruzamentoInformacoes;
using Core.Jira;
using Core.Ponto;
using Dapper;

namespace Core.Consolidacao
{
    public class BuscadorSituacaoDia : IBuscadorSituacaoDia
    {
        public IList<DataSituacao> BuscarSituacoesComPendencias(DateTime dataInicio, Credencial credencial)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var situacoes = connection
                .Query<DataSituacao>($"select * from DataSituacao where DataPonto >= '{dataInicio:yyyy-MM-dd}' and CodigoRonda={credencial.CodigoRonda} and Situacao <> {(int)StatusDia.Nenhuma}")
                .ToList();
            connection.Close();
            return situacoes;
        }

        public int BuscarOfensivaAtual(Credencial credencial)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var ofensiva = connection
                .Query<int?>($"select top 1 Ofensiva from DataSituacao where CodigoRonda={credencial.CodigoRonda} order by Id desc")
                .FirstOrDefault() ?? 0;
            connection.Close();
            return ofensiva;
        }
    }
}