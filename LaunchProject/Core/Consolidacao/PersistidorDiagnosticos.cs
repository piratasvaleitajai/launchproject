using System.Data.SqlClient;
using System.Linq;
using Core.CruzamentoInformacoes;
using Core.Ponto;
using Dapper;

namespace Core.Consolidacao
{
    public class PersistidorDiagnosticos : IPersistidorDiagnosticos
    {
        private readonly IAtualizadorUsuario _atualizadorUsuario;

        public PersistidorDiagnosticos(IAtualizadorUsuario atualizadorUsuario)
        {
            _atualizadorUsuario = atualizadorUsuario;
        }

        public void Persistir(IDiagnostico diagnostico, int codigoRonda)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var estavaEmOfensiva = connection
                .Query<bool?>($"select Ofensiva from DataSituacao where DataPonto = '{diagnostico.Data.AddDays(-1):yyyy-MM-dd}' and CodigoRonda = {codigoRonda}")
                .FirstOrDefault() ?? false;
            var situacaoDia = connection
                .Query<DataSituacao>($"select * from DataSituacao where DataPonto = '{diagnostico.Data:yyyy-MM-dd}' and CodigoRonda = {codigoRonda}")
                .FirstOrDefault();
            connection.Close();

            
            if (situacaoDia == null)
                CriarSituacao(diagnostico, codigoRonda, diagnostico.Ok());
            else
                AtualizarSituacao(situacaoDia, diagnostico, diagnostico.Ok());
            
            var manteveOfensiva = estavaEmOfensiva && diagnostico.Ok();
            if (manteveOfensiva)
                IncrementarOfensivaUsuario(codigoRonda);
            else 
                ZerarOfensivaUsuario(codigoRonda);
        }

        private void IncrementarOfensivaUsuario(int codigoRonda)
        {
            _atualizadorUsuario.IncrementarOfensiva(codigoRonda);
        }

        private void ZerarOfensivaUsuario(int codigoRonda)
        {
            _atualizadorUsuario.ZerarOfensiva(codigoRonda);
        }

        private void CriarSituacao(IDiagnostico diagnostico, int codigoRonda, bool iniciouOfensiva)
        {
            var situacao = new DataSituacao
            {
                CodigoRonda = codigoRonda,
                DataPonto = diagnostico.Data,
                Ofensiva = iniciouOfensiva,
                Situacao = (int)diagnostico.Situacao,
                DiferencaJiraEmSegundos = (int)diagnostico.Diferenca.Duracao.TotalSeconds,
                TotalJiraEmSegundos = (int)diagnostico.Jira.Duracao.TotalSeconds,
                TotalRondaEmSegundos = (int)diagnostico.Ronda.Duracao.TotalSeconds,
            };
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var insert = $"insert into DataSituacao (CodigoRonda, DataPonto, Situacao, Ofensiva, DiferencaJiraEmSegundos, TotalJiraEmSegundos, TotalRondaEmSegundos) values ({codigoRonda}, '{situacao.DataPonto:yyyy-MM-dd}', {(int) diagnostico.Situacao}, {(iniciouOfensiva ? 1 : 0)}, {situacao.DiferencaJiraEmSegundos}, {situacao.TotalJiraEmSegundos}, {situacao.TotalRondaEmSegundos});";
            connection.Execute(insert);
            connection.Close();
        }

        private void AtualizarSituacao(DataSituacao situacaoDia, IDiagnostico diagnostico, bool iniciouOfensiva)
        {
            situacaoDia.Ofensiva = iniciouOfensiva;
            situacaoDia.DiferencaJiraEmSegundos = (int) diagnostico.Diferenca.Duracao.TotalSeconds;
            situacaoDia.TotalJiraEmSegundos = (int) diagnostico.Jira.Duracao.TotalSeconds;
            situacaoDia.TotalRondaEmSegundos = (int) diagnostico.Ronda.Duracao.TotalSeconds;
            situacaoDia.Situacao = (int) diagnostico.Situacao;
            
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            connection.Execute(
                $"update DataSituacao set Ofensiva={(iniciouOfensiva ? 1 : 0)}, Situacao={situacaoDia.Situacao}, DiferencaJiraEmSegundos={situacaoDia.DiferencaJiraEmSegundos}, TotalJiraEmSegundos={situacaoDia.TotalJiraEmSegundos}, TotalRondaEmSegundos={situacaoDia.TotalRondaEmSegundos} where id={situacaoDia.Id};");
            connection.Close();
        }
    }
}