using System.Data.SqlClient;
using System.Linq;
using Core.Ponto;
using Dapper;

namespace Core.Consolidacao
{
    public class AtualizadorUsuario : IAtualizadorUsuario
    {
        public void IncrementarOfensiva(int codigoRonda)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            connection.Execute($"update Usuario set ContadorOfensiva=ContadorOfensiva+1 where CodigoRonda={codigoRonda}");
            connection.Close();
        }

        public void ZerarOfensiva(int codigoRonda)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            connection.Execute($"update Usuario set ContadorOfensiva=0 where CodigoRonda={codigoRonda}");
            connection.Close();
        }
    }
}