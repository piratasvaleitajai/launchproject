using System;

namespace Core
{
    public static class DateTimeExtensions
    {
        public static DateTime InicioPeriodoAnterior(this DateTime data)
        {
            data = data.Day < 21 
                ? data.AddMonths(-2) 
                : data.AddMonths(-1);
            
            data = new DateTime(data.Year, data.Month, 21);
            return data;
        }
    }
}