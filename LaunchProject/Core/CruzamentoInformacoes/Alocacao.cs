﻿using System;

namespace Core.CruzamentoInformacoes
{
    public class Alocacao
    {
        public TimeSpan Duracao { get; }

        public Alocacao(TimeSpan duracao)
        {
            Duracao = duracao;
        }
    }
}