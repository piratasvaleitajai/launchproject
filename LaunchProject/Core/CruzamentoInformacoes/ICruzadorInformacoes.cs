﻿using System.Collections.Generic;
using Core.Jira;
using Core.Ponto;

namespace Core.CruzamentoInformacoes
{
    public interface ICruzadorInformacoes
    {
        IDiagnostico Cruzar(MarcacaoDiaria marcacaoDiariaRonda, IEnumerable<Lancamento> lancamentos);
    }
}