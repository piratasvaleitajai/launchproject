﻿using System;
using System.Collections.Generic;

namespace Core.CruzamentoInformacoes
{
    public static class TimeSpanExtensions
    {
        public static TimeSpan Sum(this IEnumerable<TimeSpan> tempos)
        {
            var total = TimeSpan.Zero;
            foreach (var tempo in tempos)
            {
                total += tempo;
            }
            return total;
        }
    }
}