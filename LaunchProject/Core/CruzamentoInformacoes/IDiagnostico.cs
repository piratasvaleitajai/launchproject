﻿using System;

namespace Core.CruzamentoInformacoes
{
    public interface IDiagnostico
    {
        bool Ok();
        DateTime Data { get; }
        StatusDia Situacao { get; }

        Alocacao Diferenca { get; }
        Alocacao Jira { get; }
        Alocacao Ronda { get; }
    }
}