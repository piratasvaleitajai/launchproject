namespace Core.CruzamentoInformacoes
{
    public enum StatusDia
    {
        Nenhuma = 1,
        AjustePontoPendente = 2,
        AjusteJiraPendente = 3,
        AguardandoValidacao = 4,
    }
}