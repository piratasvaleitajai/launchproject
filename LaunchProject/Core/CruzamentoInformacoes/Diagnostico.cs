﻿using System;

namespace Core.CruzamentoInformacoes
{
    public class Diagnostico : IDiagnostico
    {
        public StatusDia Situacao { get; private set; }
        public DateTime Data { get; }

        public Alocacao Diferenca { get; private set; } = new Alocacao(TimeSpan.Zero);
        public Alocacao Ronda { get; private set; } = new Alocacao(TimeSpan.Zero);
        public Alocacao Jira { get; private set; } = new Alocacao(TimeSpan.Zero);

        private Diagnostico(DateTime data)
        {
            Data = data;
        }

        public static IDiagnostico SemProblemas(DateTime data, Alocacao alocacaoJira, Alocacao alocacaoRonda)
        {
            return new Diagnostico(data)
            {
                Situacao = StatusDia.Nenhuma,
                Jira = alocacaoJira,
                Ronda = alocacaoRonda
            };
        }

        public static IDiagnostico NecessarioAjustarPonto(DateTime data)
        {
            return new Diagnostico(data)
            {
                Situacao = StatusDia.AjustePontoPendente
            };
        }

        public static IDiagnostico Diferencas(DateTime data, Alocacao alocacaoJira, Alocacao alocacaoRonda, Alocacao diferenca)
        {
            return new Diagnostico(data)
            {
                Diferenca = diferenca,
                Jira = alocacaoJira,
                Ronda = alocacaoRonda,
                Situacao = StatusDia.AjusteJiraPendente
            };
        }

        public bool Ok()
        {
            return Situacao == StatusDia.Nenhuma;
        }
    }
}