﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Jira;
using Core.Ponto;

namespace Core.CruzamentoInformacoes
{
    public class CruzadorInformacoes : ICruzadorInformacoes
    {
        public IDiagnostico Cruzar(MarcacaoDiaria marcacaoDiariaRonda, IEnumerable<Lancamento> lancamentos)
        {
            if (marcacaoDiariaRonda.Status == StatusMarcacao.PossuiPendencias)
                return Diagnostico.NecessarioAjustarPonto(marcacaoDiariaRonda.Data);
            
            var comparacao = IdentificarTempoInalocado(marcacaoDiariaRonda, lancamentos);
            return comparacao.Diferenca != TimeSpan.Zero 
                ? Diagnostico.Diferencas(marcacaoDiariaRonda.Data, new Alocacao(comparacao.Jira), new Alocacao(comparacao.Ronda), new Alocacao(comparacao.Diferenca)) 
                : Diagnostico.SemProblemas(marcacaoDiariaRonda.Data, new Alocacao(comparacao.Jira), new Alocacao(comparacao.Ronda));
        }

        private Comparacao IdentificarTempoInalocado(MarcacaoDiaria marcacaoDiariaRonda, IEnumerable<Lancamento> lancamentos)
        {
            var tempoMarcado = marcacaoDiariaRonda.Total;
            var tempoLancado = lancamentos.Select(x => x.TempoLancado).Sum();
            var diferenca = tempoMarcado - tempoLancado;
            return new Comparacao(tempoLancado, tempoMarcado, diferenca);
        }

        private class Comparacao
        {
            public TimeSpan Jira { get; }
            public TimeSpan Ronda { get; }
            public TimeSpan Diferenca { get; }

            public Comparacao(TimeSpan jira, TimeSpan ronda, TimeSpan diferenca)
            {
                Jira = jira;
                Ronda = ronda;
                Diferenca = diferenca;
            }
        }
    }
}