﻿using System;

namespace Core.Ponto.Correcoes
{
    public interface IGerenciadorMarcacoes
    {
        void AdicionarMarcacao(int codigoRonda, DateTime dataHoraCorrecao);
    }
}