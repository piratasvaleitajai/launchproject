using System;
using System.Data.SqlClient;
using Dapper;

namespace Core.Ponto.Correcoes
{
    public class GerenciadorMarcacoes : IGerenciadorMarcacoes
    {
        public void AdicionarMarcacao(int codigoRonda, DateTime dataHoraCorrecao)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var insert = $"insert into MarcacaoPonto (CodigoRonda, DataMarcacao, MarcacaoRonda, MarcacaoUsuario) values ({codigoRonda}, '{dataHoraCorrecao:yyyy-MM-dd HH:mm:ss}', 0, 1);";
            connection.Execute(insert);
            connection.Close();
        }
        
        /* public int CodigoRonda { get; set; }
        public DateTime DataMarcacao { get; set; }
        public bool MarcacaoRonda { get; set; }
        public bool MarcacaoUsuario { get; set; }
        public bool SolicitacaoAjuste { get; set; }*/
    }
}