﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Core.Ponto
{
    public class BuscadorMarcacoesBanco : IBuscadorMarcacoesBanco
    {
        public IList<MarcacaoPonto> BuscarMarcacoes(DateTime dataInicio)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var marcacoesEncontradas = connection
                .Query<MarcacaoPonto>($"select * from MarcacaoPonto where DataMarcacao >= '{dataInicio:yyyy-MM-dd}'")
                .ToList();
            connection.Close();
            return marcacoesEncontradas;
        }

        public IList<MarcacaoPonto> BuscarMarcacoesDoDia(DateTime data)
        {
            var connection = new SqlConnection(BancoUtils.ConnectionString);
            connection.Open();
            var marcacoesEncontradas = connection
                .Query<MarcacaoPonto>($"select * from MarcacaoPonto where DataMarcacao >= '{data:yyyy-MM-dd}' and DataMarcacao < '{data.AddDays(1):yyyy-MM-dd}'")
                .ToList();
            connection.Close();
            return marcacoesEncontradas;
        }
    }

    public class MarcacaoPonto
    {
        public int Id { get; set; }
        public int CodigoRonda { get; set; }
        public DateTime DataMarcacao { get; set; }
        public bool MarcacaoRonda { get; set; }
        public bool MarcacaoUsuario { get; set; }
        public bool SolicitacaoAjuste { get; set; }
        public bool Ofensiva { get; set; }
        
        /*
        CREATE TABLE MarcacaoPonto (
 Id INTEGER PRIMARY KEY IDENTITY,
 CodigoRonda INTEGER,
 DataMarcacao DATETIME,
 MarcacaoRonda BIT DEFAULT 'FALSE',
 MarcacaoUsuario BIT DEFAULT 'FALSE',
 SolicitacaoAjuste BIT DEFAULT 'FALSE',
 Ofensiva INTEGER
)
        */
    }
}