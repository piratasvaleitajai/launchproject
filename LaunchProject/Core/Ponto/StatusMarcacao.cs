﻿namespace Core.Ponto
{
    public enum StatusMarcacao
    {
        Valida = 1,
        PossuiPendencias = 2
    }
}