﻿using System;
using System.Collections.Generic;

namespace Core.Ponto
{
    public interface IBuscadorMarcacoesBanco
    {
        IList<MarcacaoPonto> BuscarMarcacoes(DateTime dataInicio);
        IList<MarcacaoPonto> BuscarMarcacoesDoDia(DateTime data);
    }
}