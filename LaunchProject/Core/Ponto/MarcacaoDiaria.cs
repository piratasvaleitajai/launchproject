﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.CruzamentoInformacoes;

namespace Core.Ponto
{
    public class MarcacaoDiaria
    {
        public IList<TimeSpan> Momentos { get; }
        public DateTime Data { get; }
        public TimeSpan Total { get; }
        public StatusMarcacao Status { get; } = StatusMarcacao.Valida;

        public MarcacaoDiaria(IList<TimeSpan> momentos, DateTime data)
        {
            Momentos = momentos;
            Data = data;
            if (momentos.Count % 2 != 0)
            {
                Status = StatusMarcacao.PossuiPendencias;
                return;
            }
            
            Total = IdentificarPares(momentos)
                .Select(x => x.Duracao)
                .Sum();
        }

        private IEnumerable<Par> IdentificarPares(IEnumerable<TimeSpan> momentos)
        {
            var lista = momentos.ToList();
            for (var i = 0; i < lista.Count; i += 2)
            {
                var primeiroTempo = lista[i];
                var segundoTempo = lista[i+1];
                yield return new Par(primeiroTempo, segundoTempo);
            }
        }

        private class Par
        {
            private readonly TimeSpan _primeiroTempo;
            private readonly TimeSpan _segundoTempo;

            public Par(TimeSpan primeiroTempo, TimeSpan segundoTempo)
            {
                _primeiroTempo = primeiroTempo;
                _segundoTempo = segundoTempo;
            }

            public TimeSpan Duracao => _segundoTempo - _primeiroTempo;
        }
    }
}