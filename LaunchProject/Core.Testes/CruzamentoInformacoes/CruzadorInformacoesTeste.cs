﻿using System;
using Core.CruzamentoInformacoes;
using Core.Jira;
using Core.Testes.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace Core.Testes.CruzamentoInformacoes
{
    public class CruzadorInformacoesTeste
    {
        private ICruzadorInformacoes _cruzador;

        [SetUp]
        public void SetUp()
        {
            _cruzador = new CruzadorInformacoes();
        }
        
        [Test]
        public void cruzando_a_marcacao_de_um_funcionario_exemplar()
        {
            var marcacaoRonda = new MarcacaoBuilder()
                .Com(8.Hours())
                .Com(12.Hours())
                .Com(13.Hours(12.Minutes()))
                .Com(18.Hours())
                .Construir();

            var lancamentosJira = new[] { new LancamentoBuilder().Totalizando(8.Hours(48.Minutes())).Construir(),  };
            
            IDiagnostico diagnostico = _cruzador.Cruzar(marcacaoRonda, lancamentosJira);

            diagnostico.Ok().Should().BeTrue();
            diagnostico.Diferenca.Should().BeNull();
            diagnostico.Situacao.Should().Be(StatusDia.Nenhuma);
        }
        
        [TestCase(8, 0, 0, 48)]
        [TestCase(0, 0, 8, 48)]
        public void identificando_que_faltou_lancar_horas_no_jira(int horas, int minutos, int horasPendentes, int minutosPendentes)
        {
            var marcacaoRonda = new MarcacaoBuilder()
                .Com(8.Hours())
                .Com(12.Hours())
                .Com(13.Hours(12.Minutes()))
                .Com(18.Hours())
                .Construir();
            var lancamentosJira = new[]
            {
                new LancamentoBuilder().Totalizando(new TimeSpan(horas, minutos, 0)).Construir()
            };
            
            var diagnostico = _cruzador.Cruzar(marcacaoRonda, lancamentosJira);

            diagnostico.Ok().Should().BeFalse();
            diagnostico.Situacao.Should().Be(StatusDia.AjusteJiraPendente);
            diagnostico.Diferenca.Duracao.Should().Be(new TimeSpan(horasPendentes, minutosPendentes, 0));
        }

        [Test]
        public void diagnosticando_que_faltam_marcacoes()
        {
            var marcacaoInvalida = new MarcacaoBuilder()
                .Com(8.Hours())
                .Com(12.Hours())
                .Com(18.Hours())
                .Construir();
            var lancamentosJira = new[] { new LancamentoBuilder().Totalizando(8.Hours()).Construir() };

            var diagnostico = _cruzador.Cruzar(marcacaoInvalida, lancamentosJira);

            diagnostico.Ok().Should().BeFalse();
            diagnostico.Situacao.Should().Be(StatusDia.AjustePontoPendente);
        }
    }
}