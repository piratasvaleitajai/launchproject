﻿using System;
using Core.CruzamentoInformacoes;
using Core.Jira;

namespace Core.Testes.Builders
{
    public class LancamentoBuilder
    {
        private TimeSpan _tempo = TimeSpan.Zero;

        public Lancamento Construir()
        {
            return new Lancamento(_tempo, DateTime.Today, "comentário", 10);
        }

        public LancamentoBuilder Totalizando(TimeSpan tempo)
        {
            _tempo = tempo;
            return this;
        }
    }
}