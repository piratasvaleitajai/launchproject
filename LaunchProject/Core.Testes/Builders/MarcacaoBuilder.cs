﻿using System;
using System.Collections.Generic;
using Core.CruzamentoInformacoes;
using Core.Ponto;

namespace Core.Testes.Builders
{
    public class MarcacaoBuilder
    {
        private readonly List<TimeSpan> _momentos = new List<TimeSpan>();

        public MarcacaoDiaria Construir()
        {
            return new MarcacaoDiaria(_momentos, DateTime.Today);
        }

        public MarcacaoBuilder Com(TimeSpan momento)
        {
            _momentos.Add(momento);
            return this;
        }
    }
}