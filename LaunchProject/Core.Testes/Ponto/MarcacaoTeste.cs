﻿using System;
using Core.Ponto;
using Core.Testes.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace Core.Testes.Ponto
{
    public class MarcacaoTeste
    {
        [Test]
        public void somando_as_marcacoes_de_um_dia_vazio()
        {
            var marcacaoRonda = new MarcacaoBuilder().Construir();

            marcacaoRonda.Total.Should().Be(TimeSpan.Zero);
        }
        
        [Test]
        public void somando_as_marcacoes_do_dia()
        {
            var marcacaoRonda = new MarcacaoBuilder()
                .Com(8.Hours())
                .Com(12.Hours())
                .Com(13.Hours(12.Minutes()))
                .Com(18.Hours())
                .Construir();

            marcacaoRonda.Total.Should().Be(8.Hours(48.Minutes()));
            marcacaoRonda.Status.Should().Be(StatusMarcacao.Valida);
        }

        [Test]
        public void marcacao_invalida()
        {
            var marcacaoRonda = new MarcacaoBuilder()
                .Com(8.Hours())
                .Com(12.Hours())
                .Com(18.Hours())
                .Construir();
            
            marcacaoRonda.Total.Should().Be(TimeSpan.Zero);
            marcacaoRonda.Status.Should().Be(StatusMarcacao.PossuiPendencias);
        }
    }
}